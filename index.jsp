<html>
<head>
<title>The very first JSP</title>
<style>
    body{
        background-color: black;
        padding: 0;
        margin: 0%;
    }

    #Box{
        width: 500px;
        height: 200px;
        border: 1px solid red;
        border-radius: 20px;
        margin: 200px auto;
        color: white;
        text-align: center;
        font-family: 'Courier New', Courier, monospace;
        font-size: 20px;
        font-weight: bold;
    }

</style>
</head>
<body>

    <%@ page import = "java.text.SimpleDateFormat" %>
    <%@ page import = "java.text.DateFormat" %>
    <%@ page import = "java.util.Date" %>

    <% 

    DateFormat onlyHours = new SimpleDateFormat("HH");
    int currTime = new Integer(onlyHours.format(new Date()));

    %>

<div id="Box">
    <br><br><br>

    <p>
         <%
            if(currTime<12){

            out.println( " Wake up Lazy " );

            }

            else if(currTime>=12 && currTime<=16)
            {
                out.println("I love online classes (You can eat  during class time :) )");
            }
            else{

                out.println("It is dinner time! ");

            }

        %> 
    </p>
</div>



</body>
</html>